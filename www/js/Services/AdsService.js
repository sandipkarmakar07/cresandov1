(function() {
    angular.module('cresando').factory('AdsService', AdsService);
    AdsService.$inject = ['$http', '$location', '$rootScope', '$q', '$window', '$localStorage', 'serverUrls'];
    function AdsService($http, serverUrls, $window, $location, $localStorage, $rootScope, $q) {
        var data = {};
       
        data.uploadAdsImage = function(productData) {
            var imageURI = productData.imageUrl;
            var deferred = $q.defer();

            var ft = new FileTransfer(),
                    options = new FileUploadOptions();

            options.fileKey = "file";
            options.fileName = imageURI.substr(imageURI.lastIndexOf('/') + 1);
            var extention = options.fileName.split('.').pop();
            if (extention == 'jpeg' || extention == 'jpg')
            {
                options.mimeType = "image/jpeg";
            }
            else if (extention == 'png')
            {
                options.mimeType = "image/png";
            }
            else
            {
                options.mimeType = "image/jpeg";
            }
            var params = new Object();
            params.ad_id = productData.ad_id;
            params.feature = productData.feature;
            options.params = params;
//            options.headers = {
//                "Connection": "close"
//            };
            ft.upload(imageURI, serverUrls.uploadadspicture, win, fail, options);

            function win(r) {
                deferred.resolve(r);
            }
            function fail(error) {
                deferred.reject(error);
            }
            return deferred.promise;
        };
       
        return data;
    }
})();
