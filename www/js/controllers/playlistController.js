(function() {
    angular.module('cresando').controller('playlistController', playlistController);
    playlistController.$inject = ['$scope'];
    function playlistController($scope){
        $scope.playlists = [
          { title: 'Reggae', id: 1 },
          { title: 'Chill', id: 2 },
          { title: 'Dubstep', id: 3 },
          { title: 'Indie', id: 4 },
          { title: 'Rap', id: 5 },
          { title: 'Cowbell', id: 6 },
          { title: 'Raap', id: 7 },
          { title: 'Rape', id: 8 },
        ];
      }
})();

