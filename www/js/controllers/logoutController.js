(function() {
    angular.module('cresando').controller('logoutController', logoutController);
    logoutController.$inject = ['$scope'];
    function logoutController($scope) {
        
        var lg = this;
        $scope.$on('$ionicView.beforeEnter', function() {
            $ionicSideMenuDelegate.canDragContent(false);

            lg.logindata = {};
            lg.logindata.email = '';
            lg.logindata.password = '';  
            delete $localStorage.loginData.userPic;
            $localStorage.loginData.userPic = '';
            $localStorage.loginData.userPic = {};
            lg.logout();
        });
        
        lg.logout = function()
            {               
                delete $localStorage.loginData;
                delete $rootScope.loginData;
                delete $localStorage.lastState;
                delete $localStorage.lastStateUrl;
                
                var elem = angular.element( document.querySelector( '#userpicxxx' ) );
                elem.html("");
                
                $location.path("/app/login");
                
                
            };

    }
})();