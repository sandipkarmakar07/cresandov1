(function() {
    angular.module('cresando').controller('loginController', loginController);
    loginController.$inject = ['$scope'];
	function loginController($scope) {
        
        var rb = this;
        $scope.$on('$ionicView.beforeEnter', function() {
            $ionicSideMenuDelegate.canDragContent(false);
            
            //$localStorage.loginData = {};
            cordova.plugins.Keyboard.close();
            rb.logindata = {};
            $scope.logindata = {};
            
            rb.logindata.email = '';
            rb.logindata.password = '';
            if (typeof $localStorage.loginData !== 'undefined' && $localStorage.loginData.userId)
            {
                $location.path("/app/login");
            }
        });

        //normal login
        rb.login = function() {
            if (!rb.logindata.email)
            {
                ionicToast.show('Please Enter Email ID.', 'top', false, 2500);
            }
            else if (!rb.logindata.password)
            {
                ionicToast.show('Please Enter Password.', 'top', false, 2500);
            }
            else
            {
                $ionicLoading.show({template: '<img src="img/loader.gif">'});
                rb.logindata.login_frm = 'device';
                if( $localStorage.device_details){
                    rb.logindata.device_type = $localStorage.device_details.device_type;
                    rb.logindata.device_token = $localStorage.device_details.device_token;
                }
                UserService.login(rb.logindata).success(function(response, status)
                {
                    $ionicLoading.hide();
                    //console.log(response);
                    
                    if (status == 200) {
                        ionicToast.show(response.data.msg, 'top', false, 2500);
                        var userdata = {
                            userId: response.data.id,
                            name: response.data.full_name,
                            unique_id: response.data.unique_id,
                            userPic: response.data.logo,
                            sex: response.data.sex,
                        };
                        
                        $localStorage.loginData = userdata;
                        $rootScope.loginData = userdata;
                        $localStorage.prevPage = {prePag: 1}; 
                        //$state.go('app.dashboard');
                        if(response.data.first_login){
                            $location.path("/app/profileupdate");
                        }else{
                            $location.path("/app/dashboard");
                        }
                        
                         
                         $timeout(function(){                       
                            updateDiv();
                        },1000);
                        
                        $scope.modal.hide();                        
                    }
                    else
                    {
                        ionicToast.show(response.msg, 'top', false, 2500);
                    }
                });
            }
        };
        
        rb.register = function(){
            $scope.modal.hide();   
            $location.path("/app/register");
        }
        
        rb.forgotpas = function(){
            $scope.modal.hide();   
            $location.path("/app/forgotpass");
        }
        
        
        var updateDiv = function(){
            var elem = angular.element( document.querySelector( '#userpicxxx' ) );
            elem.html("");
            var html = '<img src="'+$localStorage.loginData.userPic+'" width="250" alt="" title="" class="img-responsive profile-image">';
            var compiledHTML = $compile(html)($scope);
            elem.append( compiledHTML );
        }
        
        
        
        //auth login
        rb.authLogin = function(loginType){
            rb.checkData = false;
            
           // console.log(loginType);
            
            if(loginType == 'gmail'){
                var provider = new firebase.auth.GoogleAuthProvider();
            }else if(loginType == 'facebook'){
                var provider = new firebase.auth.FacebookAuthProvider();
            }
                //signInWithPopup  *** signInWithRedirect
           firebase.auth().signInWithPopup(provider).then(function(result) {
                // This gives you a Google Access Token. You can use it to access the Google API.
                    var token = result.credential.accessToken;
                    // The signed-in user info.
                    var user = result.user;
                    console.log(user);
                    
                    rb.logindata.name = user.displayName;
                    rb.logindata.email = user.email;
                    rb.logindata.userPic = user.photoURL;
                    rb.logindata.login_type = loginType;
                    
                    rb.checkData = true;
                    
                    UserService.checkUser(rb.logindata).success(function(response, status)
                    {
                        if (status == 200) {
                            ionicToast.show(response.data.msg, 'top', false, 2500);
                            var userdata = {
                                userId: response.data.id,
                                name: response.data.full_name,
                                unique_id: response.data.unique_id,
                                userPic: response.data.logo,
                            };

                            $localStorage.loginData = userdata;
                            $rootScope.loginData = userdata;
                             $localStorage.prevPage = {prePag: 1}; 
                            //$state.go('app.dashboard');
                            $timeout(function(){                       
                                updateDiv();
                            },1000);

                            if(response.data.first_login){
                                $location.path("/app/profileupdate");
                            }else{
                                $location.path("/app/dashboard");
                            }
                           
                            //$scope.modal.hide();                        
                        }
                        else
                        {
                            ionicToast.show(response.msg, 'top', false, 2500);
                        }
                    });
                    
                    // ...
                }).catch(function(error) {
                // Handle Errors here.
                console.log('error >>>',error);
                    var errorCode = error.code;
                    
                    var errorMessage = error.message;
                    // The email of the user's account used.
                    var email = error.email;
                    // The firebase.auth.AuthCredential type that was used.
                    var credential = error.credential;
                // ...
              });
    
            
            
            //console.log(rb.logindata,'--------------');
        };
        
        
        //close email modal
        rb.closeLogin = function(){
            rb.loginModal.hide();
        };
        
        
        $ionicModal.fromTemplateUrl('templates/modal.html', {
            scope: $scope
          }).then(function(modal) {
            $scope.modal = modal;
          });
          
          
      /*FB LOGIN*/
      
      
   var fbLoginSuccess = function(response) {
    if (!response.authResponse){
      fbLoginError("Cannot find the authResponse");
      return;
    }

    var authResponse = response.authResponse;

    getFacebookProfileInfo(authResponse)
    .then(function(profileInfo) {
      // For the purpose of this example I will store user data on local storage
   //console.log('FACEBOOK_UP>>>>', profileInfo);
   //console.log('Facebook_pic>>>>', profileInfo.picture.data.url);
      //  rb.logindata.authResponse = authResponse,
        rb.logindata.name = profileInfo.name;
        rb.logindata.email = profileInfo.email;
        //rb.logindata.userPic = "http://graph.facebook.com/" + authResponse.userID + "/picture?type=large";
        rb.logindata.userPic = profileInfo.picture.data.url;
        rb.logindata.login_type = 'facebook';

        rb.checkData = true;
        
        rb.logindata.login_frm = 'device';
        if( $localStorage.device_details){
            rb.logindata.device_type = $localStorage.device_details.device_type;
            rb.logindata.device_token = $localStorage.device_details.device_token;
        }
        
        UserService.checkUser(rb.logindata).success(function(response, status)
        {
            if (status == 200) {
                ionicToast.show(response.data.msg, 'top', false, 2500);
                var userdata = {
                    userId: response.data.id,
                    name: response.data.full_name,
                    unique_id: response.data.unique_id,
                    userPic: response.data.logo,
                };

                $localStorage.loginData = userdata;
                $rootScope.loginData = userdata;
                 $localStorage.prevPage = {prePag: 1}; 
                 $timeout(function(){                       
                    updateDiv();
                },1000);
                //$state.go('app.dashboard');
                if(response.data.first_login){
                    $location.path("/app/profileupdate");
                }else{
                    $location.path("/app/dashboard");
                }
                //$scope.modal.hide();                        
            }
            else
            {
                ionicToast.show(response.msg, 'top', false, 2500);
            }
        });
                    
                    
//      UserService.setUser({
//        authResponse: authResponse,
//        userID: profileInfo.id,
//        name: profileInfo.name,
//        email: profileInfo.email,
//        picture : "http://graph.facebook.com/" + authResponse.userID + "/picture?type=large"
//      });
      $ionicLoading.hide();
      //$location.path("/app/dashboard");
    }, function(fail){
      // Fail get profile info
      console.log('profile info fail', fail);
    });
  };

  // This is the fail callback from the login method
  var fbLoginError = function(error){
    console.log('fbLoginError', error);
    $ionicLoading.hide();
  };

  // This method is to get the user profile info from the facebook api
  var getFacebookProfileInfo = function (authResponse) {
    var info = $q.defer();

    facebookConnectPlugin.api('/me?fields=email,name,picture&access_token=' + authResponse.accessToken, null,
      function (response) {
				console.log(response);
        info.resolve(response);
      },
      function (response) {
				console.log(response);
        info.reject(response);
      }
    );
    return info.promise;
  };

  //This method is executed when the user press the "Login with facebook" button
  $scope.facebookSignIn = function() {    
    facebookConnectPlugin.getLoginStatus(function(success){
      if(success.status === 'connected'){
        // The user is logged in and has authenticated your app, and response.authResponse supplies
        // the user's ID, a valid access token, a signed request, and the time the access token
        // and signed request each expire
        console.log('getLoginStatus', success.status);

    		// Check if we have our user saved
    		//var user = UserService.getUser('facebook');
                var user = {userID:''};

    		if(!user.userID){
			getFacebookProfileInfo(success.authResponse)
					.then(function(profileInfo) {
						// For the purpose of this example I will store user data on local storage
                               
                               
                                        //console.log('FACEBOOK_down>>>>', profileInfo);
                                       // console.log('Facebook_dn_pic>>>>', profileInfo.picture.data.url);
                                        //rb.logindata.authResponse = authResponse,
                                        rb.logindata.name = profileInfo.name;
                                        rb.logindata.email = profileInfo.email;
                                        //rb.logindata.userPic = "http://graph.facebook.com/" + authResponse.userID + "/picture?type=large";
                                        rb.logindata.userPic = profileInfo.picture.data.url;
                                        rb.logindata.login_type = 'facebook';
                                        rb.checkData = true;
                                        
                                        rb.logindata.login_frm = 'device';
                                        if( $localStorage.device_details){
                                            rb.logindata.device_type = $localStorage.device_details.device_type;
                                            rb.logindata.device_token = $localStorage.device_details.device_token;
                                        }
                                        UserService.checkUser(rb.logindata).success(function(response, status)
                                        {
                                            if (status == 200) {
                                                ionicToast.show(response.data.msg, 'top', false, 2500);
                                                var userdata = {
                                                    userId: response.data.id,
                                                    name: response.data.full_name,
                                                    unique_id: response.data.unique_id,
                                                    userPic: response.data.logo,
                                                };

                                                $localStorage.loginData = userdata;
                                                $rootScope.loginData = userdata;
                                                 $localStorage.prevPage = {prePag: 1}; 
                                                 $timeout(function(){                       
                                                    updateDiv();
                                                },1000);
                                                //$state.go('app.dashboard');
                                                if(response.data.first_login){
                                                    $location.path("/app/profileupdate");
                                                }else{
                                                    $location.path("/app/dashboard");
                                                }
                                                //$scope.modal.hide();                        
                                            }
                                            else
                                            {
                                                ionicToast.show(response.msg, 'top', false, 2500);
                                            }
                                        });
                                        
        
//						UserService.setUser({
//							authResponse: success.authResponse,
//							userID: profileInfo.id,
//							name: profileInfo.name,
//							email: profileInfo.email,
//							picture : "http://graph.facebook.com/" + success.authResponse.userID + "/picture?type=large"
//						});
//
//						$state.go('app.home');

					}, function(fail){
						// Fail get profile info
						console.log('profile info fail', fail);
					});
				}else{
					$location.path("/app/login");
				}
      } else {
        // If (success.status === 'not_authorized') the user is logged in to Facebook,
				// but has not authenticated your app
        // Else the person is not logged into Facebook,
				// so we're not sure if they are logged into this app or not.

				console.log('getLoginStatus', success.status);

				$ionicLoading.show({
                                    template: 'Logging in...'
                                  });

				// Ask the permissions you need. You can learn more about
				// FB permissions here: https://developers.facebook.com/docs/facebook-login/permissions/v2.4
        facebookConnectPlugin.login(['email', 'public_profile'], fbLoginSuccess, fbLoginError);
      }
    });
  };
      
      
      
   
        
        /*GOOGLE PLUS LOGIN HERE*/
      
   $scope.googleSignIn = function() {
    $ionicLoading.show({
      template: 'Logging in...'
    });

    window.plugins.googleplus.login(
      {},
      function (user_data) {
        // For the purpose of this example I will store user data on local storage
        
        
//        UserService.setUser({
//          userID: user_data.userId,
//          name: user_data.displayName,
//          email: user_data.email,
//          picture: user_data.imageUrl,
//          accessToken: user_data.accessToken,
//          idToken: user_data.idToken
//        });
//console.log('GMAIL Success>>>>', user_data);
        rb.logindata.authResponse = user_data.accessToken,
        rb.logindata.name = user_data.displayName;
        rb.logindata.email = user_data.email;
        rb.logindata.userPic = user_data.imageUrl;
        rb.logindata.login_type = 'gmail';

        rb.checkData = true;
        rb.logindata.login_frm = 'device';
        if( $localStorage.device_details){
            rb.logindata.device_type = $localStorage.device_details.device_type;
            rb.logindata.device_token = $localStorage.device_details.device_token;
        }
        UserService.checkUser(rb.logindata).success(function(response, status)
        {
            if (status == 200) {
                ionicToast.show(response.data.msg, 'top', false, 2500);
                var userdata = {
                    userId: response.data.id,
                    name: response.data.full_name,
                    unique_id: response.data.unique_id,
                    userPic: response.data.logo,
                };

                $localStorage.loginData = userdata;
                $rootScope.loginData = userdata;
                 $localStorage.prevPage = {prePag: 1}; 
                //$state.go('app.dashboard');
                $ionicLoading.hide();
                $timeout(function(){                       
                    updateDiv();
                },1000);
                if(response.data.first_login){
                    $location.path("/app/profileupdate");
                }else{
                    $location.path("/app/dashboard");
                }
                //$scope.modal.hide();                        
            }
            else
            {
                ionicToast.show(response.msg, 'top', false, 2500);
            }
        });

       // $ionicLoading.hide();
       // $state.go('app.home');
      },
      function (msg) {
          console.log('Gmail Erro >>', msg);
        $ionicLoading.hide();
      }
    );
  };
  
      
    }
})();