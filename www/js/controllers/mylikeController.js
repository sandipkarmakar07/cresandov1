(function() {
    angular.module('cresando').controller('mylikeController', mylikeController);
    mylikeController.$inject = ['$scope'];
    function mylikeController($scope) {
        
        var db = this;        
        $scope.curpage = 1;
        
         
        $scope.$on('$ionicView.beforeEnter', function() { 
             
            //console.log($localStorage.loginData);
            
            if ($localStorage.loginData == undefined )
            {                
                $location.path("/app/login");
                return;
            }else{
                db.userData = $localStorage.loginData;
            
                var prevPage = $localStorage.prevPage;
                $localStorage.prevPage = {prePag: 1}; 

                if(prevPage.prePag > 1){
                    $scope.curpage = prevPage.prePag;
                }

                $ionicNavBarDelegate.showBackButton(false);


                if (db.userData.image == '')
                {
                    db.userData.image = 'img/blank_profile.jpg';
                }
                $ionicLoading.show({template: '<img src="img/loader.gif">'});



                db.loadDashboard($scope.curpage);
            }
             
        });
        
       db.loadDashboard = function(pageNo){
           
           var data = {user_id: $localStorage.loginData.userId, page:pageNo};
            UserService.getMyLikeById(data).success(function(response, status) {
                
                if (response.data.total_rows > 0)
                {
                    var delegateHandle = $ionicScrollDelegate.$getByHandle('myContent');
                        delegateHandle.anchorScroll('scrollto');
   
                    db.total_page = response.data.total_rows;                    
                    db.members = response.data.member_list;
                }
                $ionicLoading.hide();
            });
       }
        
        
       db.nextPage = function(){           
           $scope.curpage = $scope.curpage + 1;
           db.loadDashboard($scope.curpage);
           
       }
       
       db.prevPage = function(){
           $scope.curpage = $scope.curpage - 1;
           db.loadDashboard($scope.curpage);
           
       }
       

    }
})();