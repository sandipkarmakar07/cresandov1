(function() {
    angular.module('cresando').controller('dashboardNewController', dashboardNewController);
    dashboardNewController.$inject = ['$scope'];
    function dashboardNewController($scope) {
        
        var db = this;        
        $scope.curpage = 1;
        $scope.loding = true;
        //$localStorage.prevPage = {};
         
        $scope.$on('$ionicView.beforeEnter', function() {              
            
            
            if ($localStorage.loginData.userId == undefined || $localStorage.loginData.userId == '')
            {                
                
                $location.path("/app/login");
                return;
            }else{
                //db.userData = $localStorage.loginData;
                $localStorage.prevPage = {prePag: 1}; 
                $location.path("/app/dashboard/1");
            }
             
        });       


    }
})();