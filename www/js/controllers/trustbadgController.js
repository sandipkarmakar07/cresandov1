(function() {
    angular.module('cresando').controller('trustbadgController', trustbadgController);
    trustbadgController.$inject = ['$scope'];
    function trustbadgController($scope) {
        
        var ib = this;  
        
        $scope.$on('$ionicView.beforeEnter', function() { 
            
            ib.productimg = [];
            ib.newproductimg = [];
            ib.trustLength = 0;
            ib.viewData = true;
            //console.log($localStorage.loginData);
            
            if ($localStorage.loginData == undefined )
            {                
                $location.path("/app/login");
                return;
            }else{
                ib.userData = $localStorage.loginData;                
                $ionicNavBarDelegate.showBackButton(false);

                const permissions = cordova.plugins.permissions;
                permissions.checkPermission(permissions.CAMERA, function( status ){
                    if ( !status.hasPermission ) {ib.getAllPermission('camera');}
                });
                permissions.checkPermission(permissions.READ_EXTERNAL_STORAGE, function( status ){
                    if ( !status.hasPermission ) {ib.getAllPermission('gallery');}
                });

                ib.loadTrustBages(ib.userData.userId);
            }
            
            //ib.productimg = [];
            //ib.setImages();
             
        });
        
        
        ib.loadTrustBages = function(userId){
             
           var data = {user_id: userId};
            UserService.trustBadgeDropdown(data).success(function(response, status) {
               // console.log(response.data);
                
                if (response.data.badges_to_upload)
                {
                    ib.viewDropdowns = response.data.badges_to_upload;
                    ib.uploaded = response.data.my_badges;
                    
                }
                $ionicLoading.hide();
            });
       }

       ib.getAllPermission = function(permissionfor){

           const permissions = cordova.plugins.permissions;
           if(permissionfor == 'gallery'){
               permissions.checkPermission(permissions.READ_EXTERNAL_STORAGE, function( status )
               {if ( status.hasPermission ) {
                   console.log("Yes :D ");
                    }
                else {
                    console.warn("No :( ");
                        permissions.requestPermission(permissions.READ_EXTERNAL_STORAGE, success, error);
                        function error() {console.warn('Camera permission is not turned on');}
                        function success( status ) {if( !status.hasPermission ) error();}
                    }
                });
            }
            if(permissionfor == 'camera'){
                permissions.checkPermission(permissions.CAMERA, function( status ){
                if ( status.hasPermission ) {
                    console.log("Yes :D ");
                }
                else {
                    console.warn("No :( ");
                    permissions.requestPermission(permissions.CAMERA, success, error);
                    function error() {
                        console.warn('Camera permission is not turned on');
                    }
                    function success( status ) {
                        if( !status.hasPermission ) error();}
                    }
                });
            }
       }
        
               
       ib.removeTrustBadge = function( index, imgid ){
           var confirmPopup = $ionicPopup.confirm({
                title: 'Remove Image',
                template: 'Are you sure?',
             });

             confirmPopup.then(function(res) {

                if (res) {
                    $ionicLoading.show({template: '<img src="img/loader.gif">'});
                    var uID = ib.userData.userId;
                    var imageRemove = {};
                    imageRemove = {user_id: uID, trust_badge_id: imgid};
                    UserService.removeTrustImg(imageRemove).success(function(response, status) {
                        //console.log(response);
                        if (response) {
                             ib.uploaded.splice(index, 1);
                             $ionicLoading.hide();
                             ionicToast.show('Trust badge has been removed.', 'middle', false, 2500);

                        }
                    })
                }
             })
       } 
        
       /*image action*/
        ib.imageAction = function(action) {
            var remainImageCount = 15 - ib.productimg.length;
            if (ib.productimg.length < 15)
            {
                if (action === 'camera')
                {
                    ib.openCamera();
                }
                else if (action === 'gallery')
                {
                    ib.openGallery(remainImageCount);
                }
            } else
            {
                ionicToast.show('Error: Maxmum 4 images is selected.', 'middle', false, 2500);
            }
        };
        /*open camera*/
        ib.openCamera = function() {
            $ionicLoading.show({template: '<img src="img/loader.gif">'});
            var cameraOptions = {
                quality: 100,
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: Camera.PictureSourceType.CAMERA,
                targetWidth: 400,
                targetHeight: 400,
                correctOrientation: true 
            };
            $cordovaCamera.getPicture(cameraOptions).then(function(imageData) {
                ib.newproductimg.push(imageData);
                //ib.setImages();
            }, function(err) {
                // error
                ionicToast.show('Error:' + err, 'middle', false, 2500);
            }).then(function() {
                $ionicLoading.hide();
            });
        };
        /*open gallery*/
        ib.openGallery = function(count) {
            $ionicLoading.show({template: '<img src="img/loader.gif">'});
            var options = {
                maximumImagesCount: count,
                /*width: 400,
                height: 400,*/
                quality: 80
            };
            
            $cordovaImagePicker.getPictures(options)
                    .then(function(results) {
                        for (var i = 0; i < 1; i++) {
                            console.log('Image name>>>> ', results[i]);
                            if(results[i] && results[i] != undefined){
                                ib.newproductimg.push(results[i]);
                            }
                                                        
                        }
                    ib.trustLength = ib.newproductimg.length;
                        //ib.setImages();
                    }, function(error) {
                        ionicToast.show('Upload image failed.Please Select image again', 'top', false, 3000);
                    })
                    .then(function() {
                        $ionicLoading.hide();
                    });
        }; 
        
        ib.removeImage = function(index) {
            //console.log(index);
            ib.newproductimg.splice(index, 1);
           // ib.setImages();
        };
        
        ib.updateTrustBadge = function(formData){
            //console.log('call');
          // console.log('FORM DATA>>>> ',formData);
           if(formData == undefined){
               ionicToast.show('All fields are mendetory.', 'top', false, 2500);
               return false;
           }else if(!ib.newproductimg[0]){
                ionicToast.show('Please Select a Image.', 'top', false, 2500);
                return false;
           }else if(!formData.trustbadgetype || formData.trustbadgetype == undefined){
                ionicToast.show('Please Select Badge type.', 'top', false, 2500);
                return false;
           }
           else{
              //console.log(formData);
             // return;
               $ionicLoading.show({template: '<img src="img/loader.gif">'});
           
                //console.log('upload start');
                var uID = ib.userData.userId;
                //console.log(uID);
               //ib.newproductimg = 'file:///data/data/com.ionicframework.cresandov1890829/cache/tmp_IMG-20161121-WA0004-443760667.jpg';
                $ionicLoading.show({template: '<img src="img/loader.gif">'});

                ib.userData = $localStorage.loginData;
                var process = [];

                for (var i = 0; i < 1; i++) {               
                         var imageElement = {};
                         imageElement = {user_id: uID, trustType:formData.trustbadgetype, imageUrl: ib.newproductimg[0]};
                         process[0] = UserService.uploadTrustImage(imageElement);
                     }

                 $q.all(process).then(function(res) {                
                     $ionicLoading.hide();
                     ib.newproductimg = [];
                     ionicToast.show(res.msg, 'top', false, 2500);
                 });
                 
                 //ib.loadTrustBages(uID);
                 $location.path("/app/trustbadgeredirect");
           }
           
           
           
       };
        
        
        
        
        
        
      }
})();

