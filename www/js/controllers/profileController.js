(function() {
    angular.module('cresando').controller('profileController', profileController);
    profileController.$inject = ['$scope'];
    function profileController($scope) {
        
        var ib = this;   
        var selectedLanguages = [];
        
         
        $scope.$on('$ionicView.beforeEnter', function() { 
             
            ib.productData = {};
            ib.productimg = [];
            ib.newproductimg = [];
            ib.viewData = true;
            //console.log($localStorage.loginData);
            
            if ($localStorage.loginData == undefined )
            {                
                $location.path("/app/login");
                return;
            }else{
                ib.userData = $localStorage.loginData;
            
              
                
                $ionicNavBarDelegate.showBackButton(false);


                if (ib.userData.image == '')
                {
                    ib.userData.image = 'img/blank_profile.jpg';
                }

                //$ionicLoading.show({template: '<img src="img/loader.gif">'});              

                ib.loadProfileDetails(ib.userData.userId);

            }
            
            //ib.productimg = [];
            //ib.setImages();
             
        });       
        
        ib.openForm = function(){            
            ib.viewData = false;
        }
        ib.closeForm = function(){
             ib.viewData = true;
        }
        
        ib.filterValue = function($event){
                if(isNaN(String.fromCharCode($event.keyCode))){
                    $event.preventDefault();
                }
        };
        
        ib.loadProfileDetails = function(profileId){
             $ionicLoading.show({template: '<img src="img/loader.gif">'});
           var data = {user_id: profileId};
            UserService.getuserWonProfileById(data).success(function(response, status) {
                //console.log(response.data);
                
                if (response.data.user_details)
                {
                    ib.viewDetails = response.data.user_details;
                    ib.viewMetaDetails = response.data.user_meta_details;
                    ib.viewMetaDetails.sex = response.data.user_details.sex;
                    
                    ib.viewGallery = response.data.gallery;
                    
                    ib.viewCountry = response.data.countries;
                    ib.viewCity = response.data.cities;
                    ib.viewCityRes = response.data.cities;
                    ib.relationStatus = response.data.metiral_status;

                    if(ib.viewMetaDetails.contres ){
                        ib.getCountryCityResOnload(ib.viewMetaDetails.contres);
                    }
                    
                    ib.bodyType = response.data.body_types;
                    ib.complexion = response.data.complexions;
                    ib.hairColor = response.data.hair_colors;
                    ib.height = response.data.heights;
                    ib.weight = response.data.weights;
                    ib.currency = response.data.currencies;
                    ib.drink = response.data.drinks;
                    ib.education = response.data.educations;
                    ib.employeed = response.data.employeed_in;
                    ib.hobby = response.data.hobbies;
                    ib.language = response.data.languages;
                    ib.occupation = response.data.occupation;
                    ib.religion = response.data.religion;
                    ib.smokes = response.data.smokes;

                    ib.first_time = response.data.first_login;
                    ib.first_time_msg = response.data.first_login_msg;

                    if(ib.first_time){
                        ionicToast.show(ib.first_time_msg, 'top', false, 10000);
                    }
                   
                    //ib.lagnghold = [{F:'Fluent'}, {A:'Average'}, {L:'Low'}, {N:'Native' }];
//                    var newobj = [];
//                    angular.forEach(response.data.user_languages, function(key,value) {                        
//                        newobj = {id:value, hold:key};
//                        selectedLanguages.push(newobj);
//                      });
                      
                    
                    ib.selectedHobby = response.data.my_hobbies;                    
                    ib.selectedLanguage = response.data.user_languages;

                    $scope.totaldays = 31;
                    $scope.totalmonths = 12;
                    $scope.totalyears = 150;

                    $scope.days = [];
                    for (var i = 0; i < $scope.totaldays; i += 1) {
                        if(i<9){
                            $scope.days.push( '0'+(i+1));
                        }else{
                            var j = i+1;
                            $scope.days.push(j.toString() );
                        }
                        
                    }

                    $scope.months = [];
                    for (var i = 0; i < $scope.totalmonths; i += 1) {
                        if(i<9){
                            $scope.months.push(  '0'+(i+1));
                        }else{
                            var j = i+1;
                            $scope.months.push(j.toString());
                        }                    
                    }

                    $scope.years = [];
                    var currentYear = new Date().getFullYear();
                    for (var i = currentYear; i > currentYear - $scope.totalyears; i--) {
                        var j = i - 1;
                        $scope.years.push(j.toString());
                    }
                    
                    
                    //console.log(selectedLanguages);
                    
                    if(ib.viewGallery.length > 0){
                        for(var i=0; i < ib.viewGallery.length; i++){
                            ib.productimg.push(ib.viewGallery[i]);
                        }
                        //ib.productimg.push(ib.viewGallery);
                    }

                     if(ib.viewDetails.dob){
                        var dob = ib.viewDetails.dob;
                        var values = dob.split("-");
                        ib.Bday = values[2];
                        ib.Bmonth = values[1];
                        ib.Byear = values[0];
                       // console.log(ib.Byear+'-'+ib.Bmonth+'-'+ib.Bday,);
                    }
                    //console.log( ib.productimg);
                }
                $ionicLoading.hide();
            });
       }
        
       
       ib.updateProfile = function(formData){
           var currentYear = new Date().getFullYear();
           var userAge = currentYear - formData.Byear;
           if(!formData.viewMetaDetails.sex){
               ionicToast.show('Please update your sex.', 'top', false, 2500);
               return false;
           }else if(userAge < 18){
                ionicToast.show('Sorry! You are underage.', 'top', false, 2500);
                return false; 
           }            
           else{           

            $scope.selHoby = [];
            $scope.selLang = [];
            angular.forEach(formData.selectedHobby, function (hobby, index) {
                //console.log(hobby);
                $scope.selHoby.push(hobby.id);
            });
            
            angular.forEach(formData.selectedLanguage, function (lang, index) {
                //console.log(lang);
                $scope.selLang.push(lang.id);
            });
            
            ib.userData = $localStorage.loginData;
            
           //console.log($scope.selectedHobby, formData.selectedLanguage);
           
           var formVar = {
                uid: ib.userData.userId,
                abtme: formData.viewMetaDetails.abtme,
                intrst: formData.viewMetaDetails.intrst,
                mhate: formData.viewMetaDetails.mhate,
                mfav: formData.viewMetaDetails.mfav,  
                contres: formData.viewMetaDetails.contres,
                citres: formData.viewMetaDetails.citres,
                adres: formData.viewMetaDetails.adres,
                zipres: formData.viewMetaDetails.zipres ,
                acc_comp: formData.viewMetaDetails.acc_comp,
                trst_comp:formData.viewMetaDetails.trst_comp,
                height: formData.viewMetaDetails.height,
                weight: formData.viewMetaDetails.weight,
                bdtyp: formData.viewMetaDetails.bdtyp,
                hircol: formData.viewMetaDetails.hircol,
                smoke: formData.viewMetaDetails.smoke,
                drink: formData.viewMetaDetails.drink,
                seek: formData.viewMetaDetails.seek,
                mrtsts: formData.viewMetaDetails.mrtsts,
                cmplxn: formData.viewMetaDetails.cmplxn,
                income: formData.viewMetaDetails.income,
                incom_type: formData.viewMetaDetails.incom_type,
                inc_cur: formData.viewMetaDetails.inc_cur,
                crncy: formData.viewMetaDetails.crncy,
                hedu: formData.viewMetaDetails.hedu,
                edudet: formData.viewMetaDetails.edudet,
                ocu: formData.viewMetaDetails.ocu,
                ocudet: formData.viewMetaDetails.ocudet,
                empin: formData.viewMetaDetails.empin,
                relgion: formData.viewMetaDetails.relgion,
                relgion_det: formData.viewMetaDetails.relgion_det,
                uhlght: formData.viewMetaDetails.uhlght,
                country: formData.viewMetaDetails.country,
                city: formData.viewMetaDetails.city,
                sex: formData.viewMetaDetails.sex,
                my_hobbies: $scope.selHoby,
                user_languages: $scope.selLang,
                dob: formData.Byear+'-'+formData.Bmonth+'-'+formData.Bday,
           };
           
           //console.log(formVar);           
           $ionicLoading.show({template: '<img src="img/loader.gif">'});
           UserService.saveUserProfile(formVar).success(function(response, status) {
              // if (response) {
              $ionicLoading.hide();
               ionicToast.show('Profile has been updated.', 'middle', false, 2500);
           })
         }
       };
        
        
       /*image action*/
        ib.imageAction = function(action) {
            var remainImageCount = 15 - ib.productimg.length;
            if (ib.productimg.length < 15)
            {
                if (action === 'camera')
                {
                    ib.openCamera();
                }
                else if (action === 'gallery')
                {
                    ib.openGallery(remainImageCount);
                }
            } else
            {
                ionicToast.show('Error: Maxmum 4 images is selected.', 'middle', false, 2500);
            }
        };
        /*open camera*/
        ib.openCamera = function() {
            $ionicLoading.show({template: '<img src="img/loader.gif">'});
//            var cameraOptions = {
//                quality: 100,
//                destinationType: Camera.DestinationType.FILE_URI,
//                sourceType: Camera.PictureSourceType.CAMERA,
//                targetWidth: 400,
//                targetHeight: 400,
//                encodingType: Camera.EncodingType.JPEG,
//                correctOrientation: true 
//            };
            
            var cameraOptions = { 
                quality: 50,
                destinationType: Camera.DestinationType.FILE_URI,
                // In this app, dynamically set the picture source, Camera or photo gallery
                sourceType: Camera.PictureSourceType.CAMERA,
                encodingType: Camera.EncodingType.JPEG,
                mediaType: Camera.MediaType.PICTURE,
                targetWidth: 400,
                targetHeight: 400,
                correctOrientation: true  //Corrects Android orientation quirks
            };
            
            $cordovaCamera.getPicture(cameraOptions).then(function(imageData) {
                ib.newproductimg.push(imageData);
                //ib.setImages();
            }, function(err) {
                // error
                ionicToast.show('Error:' + err, 'middle', false, 2500);
            }).then(function() {
                $ionicLoading.hide();
            });
        };
        /*open gallery*/
        ib.openGallery = function(count) {
            $ionicLoading.show({template: '<img src="img/loader.gif">'});
            var options = {
                maximumImagesCount: count,
                width: 400,
                height: 400,
                quality: 100
            };
            
            $cordovaImagePicker.getPictures(options)
                    .then(function(results) {
                        for (var i = 0; i < results.length; i++) {
                            ib.newproductimg.push(results[i]);
                        }
                        //ib.setImages();
                    }, function(error) {
                        ionicToast.show('Upload image failed.Please Select image again', 'top', false, 3000);
                    })
                    .then(function() {
                        $ionicLoading.hide();
                    });
        }; 
        
       /*manage Image*/
        ib.removeImage = function(index) {
            //console.log(index);
            ib.newproductimg.splice(index, 1);
           // ib.setImages();
        };
        
        ib.removePrevImage = function(index, logoId){
            
          var confirmPopup = $ionicPopup.confirm({
            title: 'Remove Image',
            template: 'Are you sure?',
         });

         confirmPopup.then(function(res) {

            if (res) {

               $ionicLoading.show({template: '<img src="img/loader.gif">'});
                var uID = ib.userData.userId;
                var imageRemove = {};
                imageRemove = {user_id: uID, gallery_id: logoId};
                UserService.removeProfileImg(imageRemove).success(function(response, status) {
                    //console.log(response);
                    if (response) {
                         //console.log(ib.productimg, '------------>>');
                        delete ib.productimg;
                         ib.productimg = [];                    
                         ib.viewGallery = {};
                         //console.log(ib.productimg, '------------');
                         ib.viewGallery = response.data.gallery;
                         //ib.productimg.splice(index, 1);

                         $timeout(function(){
                             $ionicSlideBoxDelegate.update();
                             $ionicSlideBoxDelegate.$getByHandle('myContent').update();
                             if(ib.viewGallery.length > 0){
                                 for(var i=0; i < ib.viewGallery.length; i++){
                                     ib.productimg.push(ib.viewGallery[i]);
                                 }
                                 //ib.productimg.push(ib.viewGallery);
                             }
                         },2000);

                         $ionicLoading.hide();
                         ionicToast.show('Gallery picture has been removed.', 'middle', false, 2500);

                    }
                })

            }  

         });
           
        };
        
        
        ib.makeDefultImage = function(imgid){
           $ionicLoading.show({template: '<img src="img/loader.gif">'});
           var uID = ib.userData.userId;
           var imageDefult = {};
           $scope.name = '';
           imageDefult = {user_id: uID, gallery_id: imgid};
           UserService.makeDefultProfileImg(imageDefult).success(function(response, status) {
               //console.log(response);
               
               if (response) {
                   
                    delete $localStorage.loginData.userPic;
                    $localStorage.loginData.userPic = '';
                    $localStorage.loginData.userPic = {};
                    
                    $localStorage.loginData.userPic = response.data;
                    
                        //$rootScope.$broadcast('PHOTO_UPLOADED', photo);
                    //$scope.$broadcast("name", $localStorage.loginData.userPic);
                    //
                    //$scope.$apply(); 
                      
                    $timeout(function(){
                       
                        updateDiv();
                        
                    },1000);
                    
                    
                    //console.log($localStorage.loginData.userPic);
                    
                    $ionicLoading.hide();
                    ionicToast.show('Profile picture has been updated.', 'middle', false, 2500);
                    
               }
           })
        };
        
        var updateDiv = function(){
            var elem = angular.element( document.querySelector( '#userpicxxx' ) );
            elem.html("");
            var html = '<img src="'+$localStorage.loginData.userPic+'" width="250" alt="" title="" class="img-responsive profile-image">';
            var compiledHTML = $compile(html)($scope);
            elem.append( compiledHTML );
        }
        
        ib.editGallery = function(){
            $location.path("/app/gallery");
        }
       
        /*set image*/
        ib.setImages = function() {
            if (ib.productimg[0]) {
                ib.first = ib.productimg[0];
            } else {
                ib.first = 'img/no-image.png';
            }
            if (ib.productimg[1]) {
                ib.second = ib.productimg[1];
            } else {
                ib.second = 'img/no-image.png';
            }
            if (ib.productimg[2]) {
                ib.third = ib.productimg[2];
            } else {
                ib.third = 'img/no-image.png';
            }
            if (ib.productimg[3]) {
                ib.forth = ib.productimg[3];
            } else {
                ib.forth = 'img/no-image.png';
            }
            //$scope.$apply();
        };
        
        
       
       ib.getCountryCity = function(){
           //console.log(ib.viewMetaDetails.country);
           $ionicLoading.show({template: '<img src="img/loader.gif">'});
           UserService.getCityList(ib.viewMetaDetails.country).success(function(response, status) {
               if (response) {
                   //console.log(response);
                    $ionicLoading.hide();
                    ib.viewCity = response.data;
                    //ionicToast.show('Profile has been updated.', 'middle', false, 2500);
                }
           })
       } 
        
        
       ib.getCountryCityRes = function(){
           //console.log(ib.viewMetaDetails.country);
           $ionicLoading.show({template: '<img src="img/loader.gif">'});
           UserService.getCityList(ib.viewMetaDetails.contres).success(function(response, status) {
               if (response) {
                   //console.log(response);
                    $ionicLoading.hide();
                    ib.viewCityRes = response.data;
                    //ionicToast.show('Profile has been updated.', 'middle', false, 2500);
                }
           })
       } 

       ib.getCountryCityResOnload = function(countryId){
           //console.log(ib.viewMetaDetails.country);
           $ionicLoading.show({template: '<img src="img/loader.gif">'});
           UserService.getCityList(countryId).success(function(response, status) {
               if (response) {
                   //console.log(response);
                    $ionicLoading.hide();
                    ib.viewCityRes = response.data;
                    //ionicToast.show('Profile has been updated.', 'middle', false, 2500);
                }
           })
       }
        
        ib.uploadImage = function(){
           //console.log('upload start');
           var uID = ib.userData.userId;
           //console.log(uID);
          //ib.newproductimg = 'file:///data/data/com.ionicframework.cresandov1890829/cache/tmp_IMG-20161121-WA0004-443760667.jpg';
           $ionicLoading.show({template: '<img src="img/loader.gif">'});
           
           ib.userData = $localStorage.loginData;
           var process = [];
           
           for (var i = 0; i < ib.newproductimg.length; i++) {
                    var imageElement = {};
                    imageElement = {user_id: uID, imageUrl: ib.newproductimg[i]};
                    process[i] = UserService.uploadProductImage(imageElement);
                }
                
            //console.log('Process Q',process);    
            
            $q.all(process).then(function(res) {
               // console.log('RESponse is here', res);
                $ionicLoading.hide();
                ib.newproductimg = [];
                ionicToast.show(res.msg, 'top', false, 2500);
            });
            
            $location.path("/app/profile");
       }
       
       
       

    }
})();