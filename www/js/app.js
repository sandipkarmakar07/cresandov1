// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('cresando', ['ionic', 'ionic-toast', 'ngStorage', 'ngCordova', 'firebase', 'multipleSelect'])

.run(function($ionicPlatform, $localStorage, $ionicHistory, $location, $rootScope, ionicToast, $timeout, $state, $interval, $cordovaPush, $cordovaDevice, $ionicPopup, $ionicLoading, $cordovaVibration, $stateParams) {
   
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    
    //$ionicPlatform.isFullScreen=true;

    if (window.cordova && window.cordova.plugins.Keyboard) {
        window.cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        window.cordova.plugins.Keyboard.disableScroll(true);
        cordova.plugins.Keyboard.close();
        
    }
   
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
    
    
    if(window.Connection) {
        if(navigator.connection.type == Connection.NONE) {
            $ionicPopup.confirm({
                title: "Internet Disconnected",
                content: "The internet is disconnected on your device."
            })
            .then(function(result) {
                if(!result) {
                    ionic.Platform.exitApp();
                }
            });
        }
    }
    
    if( window.cordova){
        if( $cordovaDevice.getPlatform().toLowerCase() == 'android'){
            //var androidConfig = { "senderID": "1082308898211"};
            var androidConfig = { "senderID": "833653794675"};
            $cordovaPush.register(androidConfig).then(function(result) {
                $localStorage.device_details = { device_type: $cordovaDevice.getPlatform().toLowerCase(), device_token: result};
                //console.log('success >>>> ',  $localStorage.device_details);
            }, function(err) {
                //console.log('Error: '+err);
            });
        }
        else if( $cordovaDevice.getPlatform().toLowerCase() == 'ios'){
            var iosConfig = { "badge": true, "sound": true, "alert": true};
            $cordovaPush.register(iosConfig).then(function(deviceToken) {
              $localStorage.device_details = { device_type: $cordovaDevice.getPlatform().toLowerCase(), device_token: deviceToken};
              //appsflyercustom.appUninstallTrack([deviceToken], function(){}, function(){});
            }, function(err) {
                console.log('Error: '+err);
            });
        }
    } else{
        //console.log('browser');
    }
    
    
    
     $rootScope.$on('$cordovaPush:notificationReceived', function(event, notification) {
                
                // This section for ANDROID push notification callback
                if( $cordovaDevice.getPlatform().toLowerCase() == 'android'){
                    switch(notification.event) {
                        case 'registered':
                           // console.log('reg>>', notification.regid);
                            if (notification.regid.length > 0 ) {
                                $localStorage.device_details = { device_type: $cordovaDevice.getPlatform().toLowerCase(), device_token: notification.regid}; 
                            }
                        break;

                        case 'message':
                           // console.log('Notification recieved>>', JSON.stringify(notification));
                            //var payld = notification.payload;
                           // console.log('MEssage>>', notification.regid);
                            //processNotification( notification.payload);
                            processNotification( notification);
                        break;

                        case 'error':
                            console.log('GCM error = ' + notification.msg);
                        break;

                        default:
                            console.log('An unknown GCM event has occurred');
                        break;
                    }
                }
                
                // This section for IOS push notification callback
                if( $cordovaDevice.getPlatform().toLowerCase() == 'ios'){
                    /*if (notification.badge) {
                        $cordovaPush.setBadgeNumber(notification.badge).then(function(result) {
                                console.log( "Result:"+result);
                            }, function(err) {
                                // An error occurred. Show a message to the user
                                console.log( "Error:"+err);
                        });
                    }*/

                    if (notification.alert){
                        processNotification( notification);
                    }

                    /*if (notification.sound) {
                        var snd = new Media(event.sound);
                        snd.play();
                    }*/
                }

                function processNotification( notify ){
                    var popuptitle = ( notify.payload.popuptitle && notify.payload.popuptitle != '') ? notify.payload.popuptitle : 'Welcome to Cresando';
                    var popupsubtitle = ( notify.payload.popupsubtitle && notify.payload.popupsubtitle != '') ? notify.payload.popupsubtitle : 'Cresando';
                    var popupbody = ( notify.payload.popupbody && notify.payload.popupbody != '') ? notify.payload.popupbody : "Cresando";
                    var canceltext = ( notify.payload.canceltext && notify.payload.canceltext != '') ? notify.payload.canceltext : 'Later';
                    var oktext = ( notify.payload.oktext && notify.payload.oktext != '') ? notify.payload.oktext : 'Chat Now';
                    
                    if ($localStorage.loginData) {
                        var redirectpath = ( notify.payload.redirectpath && notify.payload.redirectpath != '') ? notify.payload.redirectpath : '/app/dashboard';
                    } else {
                        var redirectpath = '/app/login';                      
                    }                        
//console.log(redirectpath);
                    if( notify.payload.showconfirmpopup && notify.payload.showconfirmpopup == 1){
                     // console.log('h1');
                        $ionicPopup.confirm({
                            title: popuptitle,
                            subTitle: popupsubtitle,
                            template: popupbody,
                            cssClass: 'push-popup-wrap',
                            cancelText: canceltext,
                            okText: oktext,
                        }).then(function(res) {
                            if (res) {
                                if($state.current.name != 'app.chatdetails')
                                    $location.path(redirectpath);
                            } else {
                              //  console.log('failure');
                            }
                        });
                    }
                    else if( notify.payload.showalertpopup && notify.payload.showalertpopup == 1){
                      // console.log('h2');
                        var alertPopup = $ionicPopup.alert({
                            title: popuptitle,
                            subTitle: popupsubtitle,
                            template: popupbody,
                            cssClass: 'push-popup-wrap',
                            okText: oktext
                        });
                        alertPopup.then(function(res) {
                            if($state.current.name != 'app.chatdetails')
                                $location.path(redirectpath);
                        });
                    } else {
                      //
                     //  console.log('h0');
                      if(!notify.foreground){
                      //   console.log('hF');
                           $location.path(redirectpath);
                      }else{
                       //  console.log('hL');
                         if($state.current.name != 'app.chatdetails'){
                              $ionicPopup.confirm({
                                  title: notify.payload.title,
                                  subTitle: popupsubtitle,
                                  template: popupbody,
                                  cssClass: 'push-popup-wrap',
                                  cancelText: canceltext,
                                  okText: oktext,
                              }).then(function(res) {
                                  if (res) {
                                       $location.path(redirectpath);
                                  } else {
                                    //  console.log('failure');
                                  }
                              });

                         }else{
                           $cordovaVibration.vibrate(100);
                         }
                         
                         //$location.path(redirectpath);
                          //$rootScope.playNotificationSound();
                          //navigator.notification.beep(2);
                      }
                     
                        //$location.path(redirectpath);
                            //$location.path('/app/dashboard');
                    }
                }               
            });
    
    
  });
  
  /*this will prevent back button functionality.*/
  
//  $ionicPlatform.registerBackButtonAction(function (event) {
//        event.preventDefault();
//    }, 100);

$ionicPlatform.registerBackButtonAction(function (event) {
  if($state.current.name == "app.dashboard" || $state.current.name == "app.login"){
     // navigator.app.backHistory();
      //console.log('App',$localStorage);
    //navigator.app.exitApp(); //<-- remove this line to disable the exit
    var page_no = $stateParams.id;
    if(page_no == 1 || $state.current.name == "app.login"){
      showConfirm();
    }else{
      navigator.app.backHistory();
    }
   
  }
  else {
    navigator.app.backHistory();
  }
}, 100);

function showConfirm() {
  var confirmPopup = $ionicPopup.show({
      title : 'See You Soon!',
      template : 'Are you sure you want to exit Cresando?',
      buttons : [{
      text : 'Nop!',
      type : 'button-positive',
      }, {
      text : 'For now',
      type : 'button-positive button-outline',
      onTap : function() {
        ionic.Platform.exitApp();
      }
    }]
  });
}
    
    
})

        
//.directive('focusMe', function($timeout) {
//  return {
//   link: function(scope, element, attrs) {
//    console.log("focus");
//     $timeout(function() {
//       element[0].focus(); 
//     },750);
//   }
//   };
//})


.factory("FB", function() {
    // Initialize Firebase
    var config = {
        //Fill this config with your firebase 3 API
        apiKey: "AIzaSyBf-SOdokCkpKw_d3s23nEcLdifEVsaR_I",
        authDomain: "cresando-187a1.firebaseapp.com",
        databaseURL: "https://cresando-187a1.firebaseio.com",
        storageBucket: "cresando-187a1.appspot.com",
        messagingSenderId: "833653794675"
    };
    return firebase.initializeApp(config);
})

.directive('onlyDigits', function () {
    return {
      require: 'ngModel',
      restrict: 'A',
      link: function (scope, element, attr, ctrl) {
        function inputValue(val) {
          if (val) {
            var digits = val.replace(/[^0-9.]/g, '');

            if (digits.split('.').length > 2) {
              digits = digits.substring(0, digits.length - 1);
            }

            if (digits !== val) {
              ctrl.$setViewValue(digits);
              ctrl.$render();
            }
            return parseFloat(digits);
          }
          return undefined;
        }            
        ctrl.$parsers.push(inputValue);
      }
    };
 })
 
.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
    
  
  $ionicConfigProvider.backButton.icon('ion-ios-arrow-thin-left');
  $ionicConfigProvider.backButton.text('');
    
  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'playlist'
  })
   
   
   .state('app.login', {
      url: '/login',
      cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/login.html',
          controller: 'login as login'
        }
      }
    })
    
    .state('app.logout', {
        url: '/logout',
        views: {
            'menuContent': {
                templateUrl: 'templates/logout.html',
                controller: 'logout as lg'
            }
        }
    })

  
  .state('app.dashboardn', {
    url: '/dashboardn',
    cache: false,
    views: {
      'menuContent': {
        templateUrl: 'templates/dashboard.html',
        controller: 'dashboardNew as dashboardn',
        
      }
    }
  })
  
  .state('app.visitor', {
    url: '/visitor',
    cache: false,
    views: {
      'menuContent': {
        templateUrl: 'templates/visitor.html',
        controller: 'visitor as visitor'
      }
    }
  })
  
  .state('app.mylike', {
    url: '/mylike',
    cache: false,
    views: {
      'menuContent': {
        templateUrl: 'templates/mylike.html',
        controller: 'mylike as mylike'
      }
    }
  })
  
 
  
  .state('app.profile', {
    url: '/profile',
    cache: false,
    views: {
      'menuContent': {
        templateUrl: 'templates/profile.html',
        controller: 'profile as profile'
      }
    }
  })
  
  .state('app.gallery', {
    url: '/gallery',
    cache: false,
    views: {
      'menuContent': {
        templateUrl: 'templates/gallery.html',
        controller: 'gallery as gal'
      }
    }
  })   
   
  .state('app.trustbadge', {
    url: '/trustbadge',
    cache: false,
    views: {
      'menuContent': {
        templateUrl: 'templates/trustbadge.html',
        controller: 'trustbadg as tbc'
      }
    }
  })
  
  
  
  
  .state('app.browse', {
      url: '/browse',
      cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/browse.html',
          controller: 'browse as brows'
        }
      }
    })   
    

  
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/dashboard/1');
})
.factory('serverUrls', serverUrls);
serverUrls.$inject = [];
function serverUrls() {
    
    var HOSTURL = 'http://cresando.com/api';
    //var HOSTURL = 'http://119.81.23.162/dev/api';
    var FIREBASELINK = 'https://cresando-187a1.firebaseio.com/';
    
    var urls = {
        //editimageurl: "http://59.162.181.92/dtswork/riderbuyapp/img/adsImage/",
        //editimageurl: "http://www.riderbuy.com.au/admin/img/adsImage/",
        firebaseLink: FIREBASELINK,
        
        login: HOSTURL + '/login',
        authcheckuser: HOSTURL + '/login_social',
        register: HOSTURL + '/register',
        forgotpassword: HOSTURL + '/forgot_password',
        getuserdashboardbyid: HOSTURL + '/dashboard',     
        getuseradsearchbyid: HOSTURL + '/advance_search_options', 
        advnssavensearch: HOSTURL + '/savesearch', 
        getuserprofiledetailbyid: HOSTURL + '/profile', 
        
        getvisitorsbyid: HOSTURL + '/profile_visitor',
        getmylikebyid: HOSTURL + '/get_my_liked',
        getlikedmebyid: HOSTURL + '/get_liked',
        getblockedbyid: HOSTURL + '/get_disliked',
        
        setlikebyid: HOSTURL + '/set_like',
        unsetlikebyid: HOSTURL + '/unset_like',
        setblockbyid: HOSTURL + '/set_dislike',
        unsetblockbyid: HOSTURL + '/unset_dislike',         
        
        userwonprofile: HOSTURL + '/edit_profile', 
        uploaduserimage: HOSTURL + '/upload_gallery',
        saveprofile: HOSTURL + '/save_profile',
        checkcont: HOSTURL + '/check_post_values_abc_xyz_horibol',
        trustbdgdropdown: HOSTURL + '/my_trust_badges',
        removetrustimg: HOSTURL + '/delete_trust_badge',
        
        makedefultimg: HOSTURL + '/gallery_set_default',
        removeprofileimg: HOSTURL + '/gallery_delete',
        getcity: HOSTURL + '/get_cities',
        
        getsettings: HOSTURL + '/setting',
        passwordupdate: HOSTURL + '/update_password',
        settingsupdate: HOSTURL + '/settings_update',
        getchatlist: HOSTURL + '/get_chatavailable_users',
        getchatdetails: HOSTURL + '/get_chat_key',
        setchatkey: HOSTURL + '/set_chat_key',
        sendpush: HOSTURL + '/set_push_notification',
        contactus: HOSTURL + '/contact_us',
        
    };
    return urls;
}
